package org.studyintonation.backend.handler

import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*
import org.studyintonation.backend.api.LessonsAPI
import org.studyintonation.backend.model.Lesson
import org.studyintonation.backend.model.RoleType
import org.studyintonation.backend.model.Task
import org.studyintonation.backend.payload.LessonRequest
import org.studyintonation.backend.payload.toLessonDB
import org.studyintonation.backend.repository.*
import java.net.URI

@Component
class LessonHandler(
    private val lessonRepository: LessonRepository,
    private val taskRepository: TaskRepository
) : LessonsAPI {
    suspend fun userCanEdit(request: ServerRequest): Boolean {
        val principal = request.awaitPrincipal() as UsernamePasswordAuthenticationToken
        return principal.authorities.contains(SimpleGrantedAuthority(RoleType.EDITOR.name)) ||
                principal.authorities.contains(SimpleGrantedAuthority(RoleType.ADMIN.name))
    }

    override suspend fun listLessons(request: ServerRequest): ServerResponse {
        val courseParam = request.queryParamOrNull("course")

        if (courseParam == null) {
            return ServerResponse.ok().json().bodyAndAwait(
                lessonRepository.findAll().map { lesson ->
                    lesson.toLesson(
                        taskRepository.findByLessonIdOrderByOrderNumber(lesson.id!!).toList().map { it.toTask() })
                }
            )
        } else {
            val courseId = courseParam.toLongOrNull()
                ?: return ServerResponse.badRequest().buildAndAwait()
            return ServerResponse.ok().json().bodyAndAwait(
                lessonRepository.findByCourseIdOrderByOrderNumber(courseId).map { lesson ->
                    lesson.toLesson(
                        taskRepository.findByLessonIdOrderByOrderNumber(lesson.id!!).toList().map { it.toTask() })
                }
            )
        }
    }

    override suspend fun getLessonById(request: ServerRequest): ServerResponse {
        val id = request.pathVariable("id").toLongOrNull()
            ?: return ServerResponse.badRequest().buildAndAwait()
        return lessonRepository.findById(id)?.let {
            ServerResponse.ok().json().bodyValueAndAwait(
                it.toLesson(taskRepository.findByLessonIdOrderByOrderNumber(it.id!!).toList().map { t -> t.toTask() })
            )
        } ?: ServerResponse.notFound().buildAndAwait()
    }

    override suspend fun createLesson(request: ServerRequest): ServerResponse {
        if (!userCanEdit(request)) return ServerResponse.status(HttpStatus.UNAUTHORIZED).buildAndAwait()

        val lessonRequest = request.awaitBodyOrNull<LessonRequest>() ?: return ServerResponse.badRequest().buildAndAwait()
        val savedLesson = lessonRepository.save(lessonRequest.toLessonDB())
        return ServerResponse.created(URI(request.uri().toString() + "/${savedLesson.id}")).buildAndAwait()
    }

    override suspend fun updateLessonById(request: ServerRequest): ServerResponse {
        if (!userCanEdit(request)) return ServerResponse.status(HttpStatus.UNAUTHORIZED).buildAndAwait()

        // Check if the lesson exists
        val id = request.pathVariable("id").toLongOrNull()
            ?: return ServerResponse.badRequest().buildAndAwait()
        val savedLesson = lessonRepository.findById(id)
            ?: return ServerResponse.notFound().buildAndAwait()

        val lessonRequest = request.awaitBodyOrNull<LessonRequest>() ?: return ServerResponse.badRequest().buildAndAwait()
        lessonRepository.save(lessonRequest.toLessonDB(savedLesson.id))
        return ServerResponse.ok().buildAndAwait()
    }

    override suspend fun deleteLessonById(request: ServerRequest): ServerResponse {
        if (!userCanEdit(request)) return ServerResponse.status(HttpStatus.UNAUTHORIZED).buildAndAwait()

        val id = request.pathVariable("id").toLongOrNull()
            ?: return ServerResponse.badRequest().buildAndAwait()
        if (!lessonRepository.existsById(id)) return ServerResponse.notFound().buildAndAwait()
        lessonRepository.deleteById(id)
        return ServerResponse.noContent().buildAndAwait()
    }
}

fun LessonDB.toLesson(tasks: List<Task>) = Lesson(
    this.id, this.courseId, this.orderNumber, this.title, this.description, this.duration, tasks
)
