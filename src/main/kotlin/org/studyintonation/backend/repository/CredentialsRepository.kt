package org.studyintonation.backend.repository

import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import org.studyintonation.backend.model.Credentials
import java.util.UUID

@Repository
interface CredentialsRepository: CoroutineCrudRepository<Credentials, UUID> {
    suspend fun findByEmail(email: String): Credentials?
}