package org.studyintonation.backend.handler

import kotlinx.coroutines.flow.map
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*
import org.studyintonation.backend.api.TasksAPI
import org.studyintonation.backend.model.RoleType
import org.studyintonation.backend.payload.*
import org.studyintonation.backend.repository.TaskRepository
import org.studyintonation.backend.repository.toTask
import java.net.URI

@Component
class TaskHandler(private val repository: TaskRepository) : TasksAPI {
    suspend fun userCanEdit(request: ServerRequest): Boolean {
        val principal = request.awaitPrincipal() as UsernamePasswordAuthenticationToken
        return principal.authorities.contains(SimpleGrantedAuthority(RoleType.EDITOR.name)) ||
                principal.authorities.contains(SimpleGrantedAuthority(RoleType.ADMIN.name))
    }
    override suspend fun listTasks(request: ServerRequest): ServerResponse {
        return ServerResponse.ok().json().bodyAndAwait(repository.findAll().map { t -> t.toTask() })
    }

    override suspend fun getTaskById(request: ServerRequest): ServerResponse {
        val id = request.pathVariable("id").toLongOrNull()
            ?: return ServerResponse.badRequest().buildAndAwait()
        return repository.findById(id)?.let {
            ServerResponse.ok().json().bodyValueAndAwait(it.toTask())
        } ?: ServerResponse.notFound().buildAndAwait()
    }

    override suspend fun createTask(request: ServerRequest): ServerResponse {
        if (!userCanEdit(request)) return ServerResponse.status(HttpStatus.UNAUTHORIZED).buildAndAwait()

        val taskRequest = request.awaitBodyOrNull<TaskRequest>() ?: return ServerResponse.badRequest().buildAndAwait()
        val savedTask = repository.save(taskRequest.toTaskDB())
        return ServerResponse.created(URI(request.uri().toString() + "/${savedTask.id}")).buildAndAwait()
    }

    override suspend fun updateTaskById(request: ServerRequest): ServerResponse {
        if (!userCanEdit(request)) return ServerResponse.status(HttpStatus.UNAUTHORIZED).buildAndAwait()

        val id = request.pathVariable("id").toLongOrNull()
            ?: return ServerResponse.badRequest().buildAndAwait()
        val savedTask = repository.findById(id)
            ?: return ServerResponse.notFound().buildAndAwait()

        val taskRequest = request.awaitBodyOrNull<TaskRequest>() ?: return ServerResponse.badRequest().buildAndAwait()
        repository.save(taskRequest.toTaskDB(savedTask.id))
        return ServerResponse.ok().buildAndAwait()
    }

    override suspend fun deleteTaskById(request: ServerRequest): ServerResponse {
        if (!userCanEdit(request)) return ServerResponse.status(HttpStatus.UNAUTHORIZED).buildAndAwait()

        val id = request.pathVariable("id").toLongOrNull()
            ?: return ServerResponse.badRequest().buildAndAwait()
        if (!repository.existsById(id)) return ServerResponse.notFound().buildAndAwait()
        repository.deleteById(id)
        return ServerResponse.noContent().buildAndAwait()
    }
}