package org.studyintonation.backend.model

import org.springframework.data.r2dbc.convert.EnumWriteSupport

enum class LanguageType {
    EN_EN,
    EN_GB,
    EN_US,
    DE_DE,
    FR_FR,
    RU_RU,
    VI_VN,
    JA_JP,
    ZH_CH,
    OTHER
}

class LanguageTypeConverter: EnumWriteSupport<LanguageType>()