package org.studyintonation.backend.repository

import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import org.studyintonation.backend.model.Account
import java.util.UUID

@Repository
interface AccountRepository: CoroutineCrudRepository<Account, UUID>