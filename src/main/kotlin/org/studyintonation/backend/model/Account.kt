package org.studyintonation.backend.model

import org.springframework.data.annotation.Id
import org.springframework.data.r2dbc.convert.EnumWriteSupport
import java.time.Instant
import java.util.UUID

enum class GenderType {
    MALE, FEMALE
}

class GenderTypeConverter: EnumWriteSupport<GenderType>()

data class Account(
    @Id val credId: UUID?,
    val displayName: String,
    val gender: GenderType,
    val birthYear: Short,
    val nativeLanguage: LanguageType,
    val sendAttempts: Boolean,
    val lastActive: Instant,
    val registered: Instant
)
