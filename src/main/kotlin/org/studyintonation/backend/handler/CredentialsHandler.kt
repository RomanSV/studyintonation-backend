package org.studyintonation.backend.handler

import org.springframework.http.HttpStatus
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*
import org.springframework.web.server.ResponseStatusException
import org.studyintonation.backend.model.Credentials
import org.studyintonation.backend.model.RoleType
import org.studyintonation.backend.payload.CredentialsRequest
import org.studyintonation.backend.repository.CredentialsRepository
import org.studyintonation.backend.security.jwt.JwtSupport


@Component
class CredentialsHandler(
    private val encoder: PasswordEncoder,
    private val repository: CredentialsRepository,
    private val jwtSupport: JwtSupport
) {

    suspend fun login(request: ServerRequest): ServerResponse {
        val credentials =
            request.awaitBodyOrNull<CredentialsRequest>() ?: return ServerResponse.badRequest().buildAndAwait()
        val user = repository.findByEmail(credentials.email)

        user?.let {
            if (encoder.matches(credentials.password, it.password)) {
                return ServerResponse.ok().json().bodyValueAndAwait(Jwt(jwtSupport.generate(it.id!!).value, it.role))
            }
        }
        throw ResponseStatusException(HttpStatus.UNAUTHORIZED)
    }

    suspend fun register(request: ServerRequest): ServerResponse {
        val credentialsRequest =
            request.awaitBodyOrNull<CredentialsRequest>() ?: return ServerResponse.badRequest().buildAndAwait()
        repository.findByEmail(credentialsRequest.email)?.also {
            return ServerResponse.status(HttpStatus.CONFLICT).buildAndAwait()
        } ?: run {
            val credentials =
                Credentials(null, credentialsRequest.email, encoder.encode(credentialsRequest.password), RoleType.USER)
            return ServerResponse.ok().json().bodyValueAndAwait(
                repository.save(credentials).let { jwtSupport.generate(it.id!!).value }
            )
        }
        return ServerResponse.badRequest().buildAndAwait()
    }
}

data class Jwt(
    val token: String,
    val role: RoleType
)