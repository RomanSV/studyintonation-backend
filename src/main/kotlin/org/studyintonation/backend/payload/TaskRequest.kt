package org.studyintonation.backend.payload

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.r2dbc.postgresql.codec.Json
import org.studyintonation.backend.model.FragmentMarkup
import org.studyintonation.backend.model.ModelType
import org.studyintonation.backend.repository.TaskDB

data class TaskRequest(
    val lessonId: Long,
    val orderNumber: Int,
    val instructions: String,
    val text: String,
    val transcription: String,
    val model: String,
    val modelType: String,
    val pitch: String,
    val markup: List<FragmentMarkup>
)

fun TaskRequest.toTaskDB(id: Long? = null) = TaskDB(
    id, lessonId, orderNumber, instructions, text, transcription, model, ModelType.valueOf(modelType), pitch,
    Json.of(jacksonObjectMapper().writeValueAsString(markup))
)