package org.studyintonation.backend.handler

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*
import org.studyintonation.backend.api.AccountAPI
import org.studyintonation.backend.repository.AccountRepository
import java.util.*

@Component
class AccountHandler(private val accountRepository: AccountRepository): AccountAPI {
    override suspend fun getAccount(request: ServerRequest): ServerResponse {
        val principal = request.awaitPrincipal() as UsernamePasswordAuthenticationToken
        return accountRepository.findById(UUID.fromString(principal.name))?.let {
            ServerResponse.ok().json().bodyValueAndAwait(it)
        } ?: ServerResponse.notFound().buildAndAwait()
    }
}