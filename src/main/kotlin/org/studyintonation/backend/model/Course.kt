package org.studyintonation.backend.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import java.time.Instant

data class Course(
    @Id val id: Long?,
    val title: String,
    val description: String,
    val logo: String,
    val language: LanguageType,
    val difficulty: Int,
    val category: String,
    val authors: List<String>,
    val active: Boolean,
    @Column("release_date") val releaseDate: Instant,
    val version: String
)