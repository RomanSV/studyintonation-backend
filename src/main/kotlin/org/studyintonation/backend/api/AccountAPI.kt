package org.studyintonation.backend.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.studyintonation.backend.model.Account

interface AccountAPI {
    @Operation(
        operationId = "getAccount", summary = "Get authorized user's account.",
        responses = [
            ApiResponse(
                responseCode = "200",
                content = [Content(array = ArraySchema(schema = Schema(implementation = Account::class)))]
            ),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
        ]
    )
    suspend fun getAccount(request: ServerRequest): ServerResponse
}