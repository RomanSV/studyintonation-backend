package org.studyintonation.backend.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.studyintonation.backend.model.Attempt
import org.studyintonation.backend.payload.AttemptRequest

interface AttemptAPI {
    @Operation(
        operationId = "listAttempts", summary = "Get all the attempts.",
        parameters = [Parameter(name = "user", `in` = ParameterIn.QUERY, description = "The user UUID", required = false)],
        responses = [
            ApiResponse(
                responseCode = "200",
                content = [Content(array = ArraySchema(schema = Schema(implementation = Attempt::class)))]
            ),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
        ]
    )
    suspend fun listAttempts(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "submitAttempt", summary = "Submit a new attempt.",
        requestBody = RequestBody(content = [Content(schema = Schema(implementation = AttemptRequest::class))]),
        responses = [
            ApiResponse(responseCode = "201", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "415", content = [Content(schema = Schema(implementation = Void::class))]),
        ]
    )
    suspend fun submitAttempt(request: ServerRequest): ServerResponse
}