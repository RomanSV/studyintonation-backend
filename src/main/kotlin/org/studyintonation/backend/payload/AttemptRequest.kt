package org.studyintonation.backend.payload

import org.studyintonation.backend.model.Attempt
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.util.*

data class AttemptRequest(
    val userId: UUID,
    val taskId: Long,
    val audio: String,
    val pitch: String,
    val timestamp: String,
    val appVersion: String
)

fun AttemptRequest.toAttempt(id: Long? = null): Attempt = Attempt(
    id, userId, taskId, audio, pitch, DateTimeFormatter.ISO_DATE_TIME.parse(timestamp, Instant::from), appVersion
)

fun AttemptRequest.toAttemptOrNull(id: Long? = null): Attempt? {
    return try {
        this.toAttempt(id)
    } catch (e: DateTimeParseException) {
        null
    }
}