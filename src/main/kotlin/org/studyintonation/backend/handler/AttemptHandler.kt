package org.studyintonation.backend.handler

import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*
import org.studyintonation.backend.api.AttemptAPI
import org.studyintonation.backend.model.RoleType
import org.studyintonation.backend.payload.*
import org.studyintonation.backend.repository.AttemptRepository
import java.net.URI
import java.util.*

@Component
class AttemptHandler(private val repository: AttemptRepository) : AttemptAPI {
    suspend fun userCanView(request: ServerRequest): Boolean {
        val principal = request.awaitPrincipal() as UsernamePasswordAuthenticationToken
        return principal.authorities.contains(SimpleGrantedAuthority(RoleType.ADMIN.name))
    }

    override suspend fun listAttempts(request: ServerRequest): ServerResponse {
        if (!userCanView(request)) return ServerResponse.status(HttpStatus.UNAUTHORIZED).buildAndAwait()

        val userParam = request.queryParamOrNull("user")

        if (userParam == null) {
            return ServerResponse.ok().json().bodyAndAwait(repository.findAll())
        } else{
            UUID.fromString(userParam)?.let {
                return ServerResponse.ok().json().bodyAndAwait(repository.findByUserIdOrderByTimestamp(it))
            } ?: return ServerResponse.badRequest().buildAndAwait()
        }
    }

    override suspend fun submitAttempt(request: ServerRequest): ServerResponse {
        val attemptRequest =
            request.awaitBodyOrNull<AttemptRequest>() ?: return ServerResponse.badRequest().buildAndAwait()
        val attempt = attemptRequest.toAttemptOrNull() ?: return ServerResponse.badRequest().buildAndAwait()
        val savedAttempt = repository.save(attempt)
        return ServerResponse.created(URI(request.uri().toString() + "/${savedAttempt.id}")).buildAndAwait()
    }
}