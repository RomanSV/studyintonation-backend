package org.studyintonation.backend.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.web.reactive.function.server.*
import org.studyintonation.backend.model.Course
import org.studyintonation.backend.payload.CourseRequest

interface CoursesAPI {
    @Operation(
        operationId = "listCourses", summary = "Get all the courses.",
        responses = [
            ApiResponse(
                responseCode = "200",
                content = [Content(array = ArraySchema(schema = Schema(implementation = Course::class)))]
            )
        ]
    )
    suspend fun listCourses(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "getCourseById", summary = "Find a course by ID.",
        parameters = [Parameter(name = "id", `in` = ParameterIn.PATH, description = "The course ID")],
        responses = [
            ApiResponse(responseCode = "200", content = [Content(schema = Schema(implementation = Course::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = Void::class))])
        ]
    )
    suspend fun getCourseById(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "createCourse", summary = "Create a new course.",
        requestBody = RequestBody(content = [Content(schema = Schema(implementation = CourseRequest::class))]),
        responses = [
            ApiResponse(responseCode = "201", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "415", content = [Content(schema = Schema(implementation = Void::class))]),
        ]
    )
    suspend fun createCourse(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "updateCourse", summary = "Updates the course with given ID.",
        parameters = [Parameter(name = "id", `in` = ParameterIn.PATH, description = "The course ID")],
        requestBody = RequestBody(content = [Content(schema = Schema(implementation = CourseRequest::class))]),
        responses = [
            ApiResponse(responseCode = "200", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "415", content = [Content(schema = Schema(implementation = Void::class))]),
        ]
    )
    suspend fun updateCourseById(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "deleteCourseById", summary = "Deletes the course with given ID.",
        parameters = [Parameter(name = "id", `in` = ParameterIn.PATH, description = "The course ID")],
        responses = [
            ApiResponse(responseCode = "204", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = Void::class))])
        ]
    )
    suspend fun deleteCourseById(request: ServerRequest): ServerResponse
}