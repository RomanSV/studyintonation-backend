package org.studyintonation.backend.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import org.studyintonation.backend.model.Attempt
import java.util.UUID

@Repository
interface AttemptRepository: CoroutineCrudRepository<Attempt, Long> {
    fun findByUserIdOrderByTimestamp(userId: UUID): Flow<Attempt>

    fun findByTaskIdOrderByTimestamp(taskId: Long): Flow<Attempt>
}