package org.studyintonation.backend.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface LessonRepository: CoroutineCrudRepository<LessonDB, Long> {
    fun findByCourseIdOrderByOrderNumber(courseId: Long): Flow<LessonDB>
}

@Table("lesson")
data class LessonDB(
    @Id val id: Long?,
    val courseId: Long,
    val orderNumber: Int,
    val title: String,
    val description: String,
    val duration: Int
)
