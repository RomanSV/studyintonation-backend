package org.studyintonation.backend.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.studyintonation.backend.model.Task
import org.studyintonation.backend.payload.TaskRequest

interface TasksAPI {
    @Operation(
        operationId = "listTasks", summary = "Get all the tasks.",
        responses = [
            ApiResponse(
                responseCode = "200",
                content = [Content(array = ArraySchema(schema = Schema(implementation = Task::class)))]
            )
        ]
    )
    suspend fun listTasks(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "getTaskById", summary = "Find a task by ID.",
        parameters = [Parameter(name = "id", `in` = ParameterIn.PATH, description = "The task ID")],
        responses = [
            ApiResponse(responseCode = "200", content = [Content(schema = Schema(implementation = Task::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = Void::class))])
        ]
    )
    suspend fun getTaskById(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "createTask", summary = "Create a new task.",
        requestBody = RequestBody(content = [Content(schema = Schema(implementation = TaskRequest::class))]),
        responses = [
            ApiResponse(responseCode = "201", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "415", content = [Content(schema = Schema(implementation = Void::class))]),
        ]
    )
    suspend fun createTask(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "updateTask", summary = "Updates the task with given ID.",
        parameters = [Parameter(name = "id", `in` = ParameterIn.PATH, description = "The task ID")],
        requestBody = RequestBody(content = [Content(schema = Schema(implementation = TaskRequest::class))]),
        responses = [
            ApiResponse(responseCode = "200", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "415", content = [Content(schema = Schema(implementation = Void::class))]),
        ]
    )
    suspend fun updateTaskById(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "deleteTaskById", summary = "Deletes the task with given ID.",
        parameters = [Parameter(name = "id", `in` = ParameterIn.PATH, description = "The task ID")],
        responses = [
            ApiResponse(responseCode = "204", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = Void::class))])
        ]
    )
    suspend fun deleteTaskById(request: ServerRequest): ServerResponse
}