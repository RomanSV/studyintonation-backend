package org.studyintonation.backend.handler

import org.springframework.http.HttpStatus
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*
import org.studyintonation.backend.api.CoursesAPI
import org.studyintonation.backend.model.RoleType
import org.studyintonation.backend.payload.CourseRequest
import org.studyintonation.backend.payload.toCourseOrNull
import org.studyintonation.backend.repository.CourseRepository
import java.net.URI

@Component
class CourseHandler(private val repository: CourseRepository) : CoursesAPI {

    suspend fun userCanEdit(request: ServerRequest): Boolean {
        val principal = request.awaitPrincipal() as UsernamePasswordAuthenticationToken
        return principal.authorities.contains(SimpleGrantedAuthority(RoleType.EDITOR.name)) ||
                principal.authorities.contains(SimpleGrantedAuthority(RoleType.ADMIN.name))
    }

    override suspend fun listCourses(request: ServerRequest): ServerResponse {
        return ServerResponse.ok().json().bodyAndAwait(repository.findAll())
    }

    override suspend fun getCourseById(request: ServerRequest): ServerResponse {
        val courseId = request.pathVariable("id").toLongOrNull()
            ?: return ServerResponse.badRequest().buildAndAwait()
        return repository.findById(courseId)?.let {
            ServerResponse.ok().json().bodyValueAndAwait(it)
        } ?: ServerResponse.notFound().buildAndAwait()
    }

    override suspend fun createCourse(request: ServerRequest): ServerResponse {
        if (!userCanEdit(request)) return ServerResponse.status(HttpStatus.UNAUTHORIZED).buildAndAwait()

        val courseRequest = request.awaitBodyOrNull<CourseRequest>() ?: return ServerResponse.badRequest().buildAndAwait()
        val course = courseRequest.toCourseOrNull() ?: return ServerResponse.badRequest().buildAndAwait()
        val savedCourse = repository.save(course)
        return ServerResponse.created(URI(request.uri().toString() + "/${savedCourse.id}")).buildAndAwait()
    }

    override suspend fun updateCourseById(request: ServerRequest): ServerResponse {
        if (!userCanEdit(request)) return ServerResponse.status(HttpStatus.UNAUTHORIZED).buildAndAwait()

        val courseId = request.pathVariable("id").toLongOrNull()
            ?: return ServerResponse.badRequest().buildAndAwait()
        if (!repository.existsById(courseId)) return ServerResponse.notFound().buildAndAwait()

        val courseRequest = request.awaitBodyOrNull<CourseRequest>() ?: return ServerResponse.badRequest().buildAndAwait()
        val course = courseRequest.toCourseOrNull(courseId) ?: return ServerResponse.badRequest().buildAndAwait()
        repository.save(course)
        return ServerResponse.ok().buildAndAwait()
    }

    override suspend fun deleteCourseById(request: ServerRequest): ServerResponse {
        if (!userCanEdit(request)) return ServerResponse.status(HttpStatus.UNAUTHORIZED).buildAndAwait()

        val courseId = request.pathVariable("id").toLongOrNull()
            ?: return ServerResponse.badRequest().buildAndAwait()
        if (!repository.existsById(courseId)) return ServerResponse.notFound().buildAndAwait()
        repository.deleteById(courseId)
        return ServerResponse.noContent().buildAndAwait()
    }
}