package org.studyintonation.backend.model

import org.springframework.data.r2dbc.convert.EnumWriteSupport

enum class ModelType {
    AUDIO,
    VIDEO
}

class ModelTypeConverter: EnumWriteSupport<ModelType>()

data class Task(
    val id: Long?,
    val lessonId: Long,
    val orderNumber: Int,
    val instructions: String,
    val text: String,
    val transcription: String,
    val model: String,
    val modelType: ModelType,
    val pitch: String,
    val markup: List<FragmentMarkup>
)