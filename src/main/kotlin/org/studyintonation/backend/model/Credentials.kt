package org.studyintonation.backend.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.r2dbc.convert.EnumWriteSupport
import java.util.*

enum class RoleType {
    USER, EDITOR, ADMIN
}

class RoleTypeConverter: EnumWriteSupport<RoleType>()

data class Credentials(
    @Id val id: UUID?,
    val email: String,
    @JsonIgnore val password: String,

    val role: RoleType
)
