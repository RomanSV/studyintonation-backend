package org.studyintonation.backend.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.web.reactive.function.server.*
import org.studyintonation.backend.model.Lesson
import org.studyintonation.backend.payload.LessonRequest


interface LessonsAPI {
    @Operation(
        operationId = "listLessons", summary = "Retrieves lessons.",
        parameters = [Parameter(name = "course", `in` = ParameterIn.QUERY, description = "The course ID", required = false)],
        responses = [
            ApiResponse(
                responseCode = "200",
                content = [Content(array = ArraySchema(schema = Schema(implementation = Lesson::class)))]
            ),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))])
        ]
    )
    suspend fun listLessons(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "getLessonById", summary = "Find a lesson by id.",
        parameters = [
            Parameter(name = "id", `in` = ParameterIn.PATH, description = "The lesson ID")
        ],
        responses = [
            ApiResponse(responseCode = "200", content = [Content(schema = Schema(implementation = Lesson::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = Void::class))])
        ]
    )
    suspend fun getLessonById(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "createLesson", summary = "Create a new lesson.",
        requestBody = RequestBody(content = [Content(schema = Schema(implementation = LessonRequest::class))]),
        responses = [
            ApiResponse(responseCode = "201", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "415", content = [Content(schema = Schema(implementation = Void::class))])
        ]
    )
    suspend fun createLesson(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "updateLessonById", summary = "Update a lesson with given id.",
        parameters = [
            Parameter(name = "id", `in` = ParameterIn.PATH, description = "The lesson ID")
        ],
        requestBody = RequestBody(content = [Content(schema = Schema(implementation = LessonRequest::class))]),
        responses = [
            ApiResponse(responseCode = "200", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "415", content = [Content(schema = Schema(implementation = Void::class))])
        ]
    )
    suspend fun updateLessonById(request: ServerRequest): ServerResponse

    @Operation(
        operationId = "deleteLessonById", summary = "Deletes the lesson with given ID.",
        parameters = [Parameter(name = "id", `in` = ParameterIn.PATH, description = "The lesson ID")],
        responses = [
            ApiResponse(responseCode = "204", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "401", content = [Content(schema = Schema(implementation = Void::class))]),
            ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = Void::class))])
        ]
    )
    suspend fun deleteLessonById(request: ServerRequest): ServerResponse
}