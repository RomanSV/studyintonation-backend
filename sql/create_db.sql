CREATE TYPE model_type AS ENUM ('AUDIO', 'VIDEO');

CREATE TYPE role_type AS ENUM ('USER', 'EDITOR', 'ADMIN');

CREATE TYPE gender_type AS ENUM ('MALE', 'FEMALE');

CREATE TYPE language_type AS ENUM
    ('EN_EN', 'EN_GB', 'EN_US', 'DE_DE', 'FR_FR', 'RU_RU', 'VI_VN', 'JA_JP', 'ZH_CH', 'OTHER');


CREATE TABLE IF NOT EXISTS credentials
(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    email TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "role" role_type NOT NULL
);

CREATE TABLE IF NOT EXISTS account
(
    cred_id UUID PRIMARY KEY,
    display_name TEXT NOT NULL,
    birth_year SMALLINT NOT NULL,
    gender gender_type NOT NULL,
    native_language language_type NOT NULL,
    send_attempts BOOLEAN NOT NULL,
    last_active TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    registered TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    CONSTRAINT cred_id_fkey FOREIGN KEY (cred_id) REFERENCES credentials (id)
);

CREATE TABLE IF NOT EXISTS course
(
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR(32) NOT NULL,
    description VARCHAR(256) NOT NULL,
    logo VARCHAR(256) NOT NULL,
    difficulty SMALLINT NOT NULL,
    language language_type NOT NULL,
    category VARCHAR(64) NOT NULL,
    authors VARCHAR(64)[] NOT NULL,
    active BOOLEAN NOT NULL,
    release_date TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    last_updated TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    version VARCHAR(16) NOT NULL
);

CREATE TABLE IF NOT EXISTS lesson
(
    id BIGSERIAL PRIMARY KEY,
    course_id BIGINT NOT NULL,
    title VARCHAR(64) NOT NULL,
    description VARCHAR(256) NOT NULL,
    duration INTEGER NOT NULL,
    order_number INTEGER NOT NULL,
    CONSTRAINT lesson_number_uc UNIQUE (course_id, order_number),
    CONSTRAINT course_id_fk FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS task
(
    id BIGSERIAL PRIMARY KEY,
    lesson_id BIGINT NOT NULL,
    instructions VARCHAR(512) NOT NULL,
    text VARCHAR(256) NOT NULL,
    transcription VARCHAR(256) NOT NULL,
    model_type model_type,
    audio VARCHAR(256) NOT NULL,
    order_number INTEGER NOT NULL,
    markup JSONB NOT NULL,
    pitch VARCHAR(256) NOT NULL,
    CONSTRAINT task_number_uc UNIQUE (lesson_id, order_number),
    CONSTRAINT lesson_id_fk FOREIGN KEY (lesson_id) REFERENCES lesson (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS attempt
(
    id BIGSERIAL PRIMARY KEY,
    user_id UUID NOT NULL,
    task_id BIGINT NOT NULL,
    audio TEXT NOT NULL,
    pitch TEXT NOT NULL,
    "timestamp" TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    app_version TEXT NOT NULL,
    CONSTRAINT task_id_fk FOREIGN KEY (task_id) REFERENCES task (id)
);
