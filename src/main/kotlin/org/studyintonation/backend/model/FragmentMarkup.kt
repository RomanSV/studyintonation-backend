package org.studyintonation.backend.model

data class FragmentMarkup(
    val fragment: String,
    val start: Double,
    val stop: Double,
    val catchword: Boolean
)
