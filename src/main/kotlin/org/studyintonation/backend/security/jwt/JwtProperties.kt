package org.studyintonation.backend.security.jwt

import org.springframework.boot.context.properties.ConfigurationProperties


@ConfigurationProperties(prefix = "jwt")
data class JwtProperties(
    val secret: String,
    val expirationInMs: Long
)
