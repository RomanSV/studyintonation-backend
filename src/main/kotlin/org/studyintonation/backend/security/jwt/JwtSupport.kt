package org.studyintonation.backend.security.jwt

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.stereotype.Component
import org.studyintonation.backend.model.Credentials
import java.time.Instant
import java.util.*
import javax.crypto.SecretKey

class BearerToken(val value: String): AbstractAuthenticationToken(AuthorityUtils.NO_AUTHORITIES) {
    override fun getCredentials(): Any = value
    override fun getPrincipal(): Any = value
}

@Component
class JwtSupport(private val properties: JwtProperties) {
    private val key: SecretKey = Keys.hmacShaKeyFor(properties.secret.toByteArray())
    private val parser = Jwts.parserBuilder().setSigningKey(key).build()

    fun generate(uuid: UUID): BearerToken {
        val builder = Jwts.builder()
            .setSubject(uuid.toString())
            .setIssuedAt(Date.from(Instant.now()))
            .setExpiration(Date.from(Instant.now().plusMillis(properties.expirationInMs)))
            .signWith(key)
        return BearerToken(builder.compact())
    }

    fun getSubject(token: BearerToken): String = parser.parseClaimsJws(token.value).body.subject

    fun isValid(token: BearerToken, credentials: Credentials?): Boolean {
        val claims = parser.parseClaimsJws(token.value).body
        val unexpired = claims.expiration.after(Date.from(Instant.now()))
        return unexpired && (claims.subject == credentials?.id.toString())
    }
}