package org.studyintonation.backend.payload

data class CredentialsRequest(
    val email: String,
    val password: String
)