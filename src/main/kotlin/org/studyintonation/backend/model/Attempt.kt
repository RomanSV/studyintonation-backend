package org.studyintonation.backend.model

import org.springframework.data.annotation.Id
import java.time.Instant
import java.util.UUID

data class Attempt(
    @Id val id: Long?,
    val userId: UUID,
    val taskId: Long,
    val audio: String,
    val pitch: String,
    val timestamp: Instant,
    val appVersion: String
)