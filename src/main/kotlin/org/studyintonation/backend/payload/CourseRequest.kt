package org.studyintonation.backend.payload

import org.studyintonation.backend.model.Course
import org.studyintonation.backend.model.LanguageType
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

data class CourseRequest(
    val title: String,
    val description: String,
    val logo: String,
    val language: LanguageType,
    val difficulty: Int,
    val category: String,
    val authors: List<String>,
    val active: Boolean,
    val releaseDate: String,
    val version: String
)

fun CourseRequest.toCourse(id: Long? = null): Course = Course(
    id, title, description, logo, language, difficulty, category, authors, active,
    DateTimeFormatter.ISO_DATE_TIME.parse(releaseDate, Instant::from), version
)

fun CourseRequest.toCourseOrNull(id: Long? = null): Course? {
    return try {
        this.toCourse(id)
    } catch (e: DateTimeParseException) {
        null
    }
}
