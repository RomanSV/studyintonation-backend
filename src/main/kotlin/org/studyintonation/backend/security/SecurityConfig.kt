package org.studyintonation.backend.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.authentication.AuthenticationWebFilter
import org.studyintonation.backend.security.jwt.JwtAuthenticationManager
import org.studyintonation.backend.security.jwt.JwtServerAuthenticationConverter


@Configuration
@EnableWebFluxSecurity
class SecurityConfig {

    @Bean
    fun passwordEncoder(): BCryptPasswordEncoder = BCryptPasswordEncoder()

    @Bean
    fun configure(
        http: ServerHttpSecurity,
        authenticationManager: JwtAuthenticationManager,
        converter: JwtServerAuthenticationConverter
    ): SecurityWebFilterChain {
        val filter = AuthenticationWebFilter(authenticationManager)
        filter.setServerAuthenticationConverter(converter)
        return http
            .authorizeExchange()
            .pathMatchers(HttpMethod.OPTIONS).permitAll()
            .pathMatchers(HttpMethod.POST, "/login", "/register").permitAll()
            .pathMatchers(
                HttpMethod.GET, "/courses/**", "/lessons/**", "/tasks/**",
                "/swagger-ui.html", "/webjars/swagger-ui/**", "/v3/api-docs/**"
            ).permitAll()
            .anyExchange().authenticated()
            .and()
            .addFilterAt(filter, SecurityWebFiltersOrder.AUTHENTICATION)
            .csrf().disable()
            .httpBasic().disable()
            .formLogin().disable()
            .build()
    }
}