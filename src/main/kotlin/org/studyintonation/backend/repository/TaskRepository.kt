package org.studyintonation.backend.repository

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.r2dbc.postgresql.codec.Json
import kotlinx.coroutines.flow.Flow
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import org.studyintonation.backend.model.ModelType
import org.studyintonation.backend.model.Task

@Repository
interface TaskRepository: CoroutineCrudRepository<TaskDB, Long> {
    fun findByLessonIdOrderByOrderNumber(lessonId: Long): Flow<TaskDB>
}

@Table("task")
data class TaskDB(
    @Id val id: Long?,
    val lessonId: Long,
    val orderNumber: Int,
    val instructions: String,
    val text: String,
    val transcription: String,
    val model: String,
    val modelType: ModelType,
    val pitch: String,
    @Column("markup") val markup: Json
)

fun TaskDB.toTask(): Task = Task(
    this.id, this.lessonId, this.orderNumber, this.instructions, this.text,
    this.transcription, this.model, this.modelType, this.pitch,
    jacksonObjectMapper().readValue(this.markup.asString())
)