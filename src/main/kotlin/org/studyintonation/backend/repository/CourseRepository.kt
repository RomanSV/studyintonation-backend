package org.studyintonation.backend.repository

import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import org.studyintonation.backend.model.Course

@Repository
interface CourseRepository: CoroutineCrudRepository<Course, Long>