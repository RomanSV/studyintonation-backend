package org.studyintonation.backend.payload

import org.studyintonation.backend.repository.LessonDB

data class LessonRequest(
    val courseId: Long,
    val orderNumber: Int,
    val title: String,
    val description: String,
    val duration: Int
)

fun LessonRequest.toLessonDB(id: Long? = null) = LessonDB(
    id, courseId, orderNumber, title, description, duration
)