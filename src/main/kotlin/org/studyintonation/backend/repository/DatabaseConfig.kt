package org.studyintonation.backend.repository

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration
import io.r2dbc.postgresql.PostgresqlConnectionFactory
import io.r2dbc.postgresql.client.SSLMode
import io.r2dbc.postgresql.codec.EnumCodec
import io.r2dbc.spi.ConnectionFactory
import io.r2dbc.spi.ConnectionFactoryOptions.*
import org.springframework.boot.autoconfigure.r2dbc.R2dbcProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration
import org.studyintonation.backend.model.*

@Configuration
class DatabaseConfig(private val properties: R2dbcProperties): AbstractR2dbcConfiguration() {

    @Bean
    override fun connectionFactory(): ConnectionFactory {
        val baseOptions = parse(properties.url)
        return PostgresqlConnectionFactory(
            PostgresqlConnectionConfiguration.builder()
                .host(baseOptions.getRequiredValue(HOST) as String)
                .port(baseOptions.getRequiredValue(PORT) as Int)
                .database(baseOptions.getRequiredValue(DATABASE) as String)
                .username(baseOptions.getRequiredValue(USER) as String)
                .password(baseOptions.getRequiredValue(PASSWORD) as String)
                .codecRegistrar(EnumCodec.builder()
                    .withEnum("role_type", RoleType::class.java)
                    .withEnum("gender_type", GenderType::class.java)
                    .withEnum("model_type", ModelType::class.java)
                    .withEnum("language_type", LanguageType::class.java)
                    .build())
                .also {
                    if (properties.properties.containsKey("sslMode"))
                        it.sslMode(SSLMode.valueOf(properties.properties["sslMode"]!!))
                }
                .build()
        )
    }

    override fun getCustomConverters(): List<Any> {
        return listOf(RoleTypeConverter(), GenderTypeConverter(), ModelTypeConverter(), LanguageTypeConverter())
    }
}