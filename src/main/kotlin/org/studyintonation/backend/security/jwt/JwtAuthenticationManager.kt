package org.studyintonation.backend.security.jwt

import kotlinx.coroutines.reactor.mono
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import org.studyintonation.backend.repository.CredentialsRepository
import reactor.core.publisher.Mono
import java.util.*

@Component
class JwtAuthenticationManager(
    private val jwtSupport: JwtSupport,
    private val credentialsRepository: CredentialsRepository
) : ReactiveAuthenticationManager {
    override fun authenticate(authentication: Authentication?): Mono<Authentication> {
        return Mono.justOrEmpty(authentication)
            .filter { auth -> auth is BearerToken }
            .cast(BearerToken::class.java)
            .flatMap { token -> mono { validate(token) } }
            .onErrorMap { error: Throwable ->
                InvalidBearerToken(
                    error.message
                )
            }
    }

    private suspend fun validate(token: BearerToken): Authentication {
        val uuid = jwtSupport.getSubject(token)

        return credentialsRepository.findById(UUID.fromString(uuid))?.let {
            if (jwtSupport.isValid(token, it)) {
                UsernamePasswordAuthenticationToken(it.id.toString(), it.password, setOf(it.role).map{r-> SimpleGrantedAuthority(r.name)})
            } else {
                error(IllegalArgumentException("Invalid user"))
            }
        } ?: error(IllegalArgumentException("No such user."))
    }
}

class InvalidBearerToken(message: String?) : AuthenticationException(message)
