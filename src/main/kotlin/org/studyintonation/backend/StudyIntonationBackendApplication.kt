package org.studyintonation.backend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan("org.studyintonation.backend")
class StudyIntonationBackendApplication

fun main(args: Array<String>) {
    runApplication<StudyIntonationBackendApplication>(*args)
}
