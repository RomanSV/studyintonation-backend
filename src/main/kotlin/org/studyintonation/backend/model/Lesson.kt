package org.studyintonation.backend.model

data class Lesson(
    val id: Long?,
    val courseId: Long,
    val orderNumber: Int,
    val title: String,
    val description: String,
    val duration: Int,
    val tasks: List<Task>
)