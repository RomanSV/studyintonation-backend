package org.studyintonation.backend

import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.coRouter
import org.studyintonation.backend.handler.*

const val coursesBase = "/courses"
const val lessonsBase = "/lessons"
const val tasksBase = "/tasks"
const val attemptsBase = "/attempts"
const val accountBase = "/account"

@Configuration
class Router {
    @RouterOperations(
        RouterOperation(path = coursesBase, method = [RequestMethod.GET], beanClass = CourseHandler::class, beanMethod = "listCourses"),
        RouterOperation(path = "$coursesBase/{id}", method = [RequestMethod.GET], beanClass = CourseHandler::class, beanMethod = "getCourseById"),
        RouterOperation(path = lessonsBase, method = [RequestMethod.GET], beanClass = LessonHandler::class, beanMethod = "listLessons"),
        RouterOperation(path = "$lessonsBase/{id}", method = [RequestMethod.GET], beanClass = LessonHandler::class, beanMethod = "getLessonById"),
        RouterOperation(path = tasksBase, method = [RequestMethod.GET], beanClass = TaskHandler::class, beanMethod = "listTasks"),
        RouterOperation(path = "$tasksBase/{id}", method = [RequestMethod.GET], beanClass = TaskHandler::class, beanMethod = "getTaskById"),
        RouterOperation(path = coursesBase, method = [RequestMethod.POST], beanClass = CourseHandler::class, beanMethod = "createCourse"),
        RouterOperation(path = "$coursesBase/{id}", method = [RequestMethod.PUT], beanClass = CourseHandler::class, beanMethod = "updateCourseById"),
        RouterOperation(path = "$coursesBase/{id}", method = [RequestMethod.DELETE], beanClass = CourseHandler::class, beanMethod = "deleteCourseById"),
        RouterOperation(path = lessonsBase, method = [RequestMethod.POST], beanClass = LessonHandler::class, beanMethod = "createLesson"),
        RouterOperation(path = "$lessonsBase/{id}", method = [RequestMethod.PUT], beanClass = LessonHandler::class, beanMethod = "updateLessonById"),
        RouterOperation(path = "$lessonsBase/{id}", method = [RequestMethod.DELETE], beanClass = LessonHandler::class, beanMethod = "deleteLessonById"),
        RouterOperation(path = tasksBase, method = [RequestMethod.POST], beanClass = TaskHandler::class, beanMethod = "createTask"),
        RouterOperation(path = "$tasksBase/{id}", method = [RequestMethod.PUT], beanClass = TaskHandler::class, beanMethod = "updateTaskById"),
        RouterOperation(path = "$tasksBase/{id}", method = [RequestMethod.DELETE], beanClass = TaskHandler::class, beanMethod = "deleteTaskById"),
        RouterOperation(path = attemptsBase, method = [RequestMethod.GET], beanClass = AttemptHandler::class, beanMethod = "listAttempts"),
        RouterOperation(path = attemptsBase, method = [RequestMethod.POST], beanClass = AttemptHandler::class, beanMethod = "submitAttempt"),
        RouterOperation(path = accountBase, method = [RequestMethod.GET], beanClass = AccountHandler::class, beanMethod = "getAccount"),
        )
    @Bean
    fun routerFunction(
        courseHandler: CourseHandler, lessonHandler: LessonHandler, taskHandler: TaskHandler,
        credentialsHandler: CredentialsHandler, accountHandler: AccountHandler, attemptHandler: AttemptHandler
    ): RouterFunction<*> = coRouter {
        GET(coursesBase, accept(APPLICATION_JSON), courseHandler::listCourses)
        GET("$coursesBase/{id}", accept(APPLICATION_JSON), courseHandler::getCourseById)
        GET(lessonsBase, accept(APPLICATION_JSON), lessonHandler::listLessons)
        GET("$lessonsBase/{id}", accept(APPLICATION_JSON), lessonHandler::getLessonById)
        GET(tasksBase, accept(APPLICATION_JSON), taskHandler::listTasks)
        GET("$tasksBase/{id}", accept(APPLICATION_JSON), taskHandler::getTaskById)
        GET(attemptsBase, accept(APPLICATION_JSON), attemptHandler::listAttempts)

        POST(coursesBase, accept(APPLICATION_JSON), courseHandler::createCourse)
        PUT("$coursesBase/{id}", accept(APPLICATION_JSON), courseHandler::updateCourseById)
        DELETE("$coursesBase/{id}", accept(APPLICATION_JSON), courseHandler::deleteCourseById)

        POST(lessonsBase, accept(APPLICATION_JSON), lessonHandler::createLesson)
        PUT("$lessonsBase/{id}", accept(APPLICATION_JSON), lessonHandler::updateLessonById)
        DELETE("$lessonsBase/{id}", accept(APPLICATION_JSON), lessonHandler::deleteLessonById)

        POST(tasksBase, accept(APPLICATION_JSON), taskHandler::createTask)
        PUT("$tasksBase/{id}", accept(APPLICATION_JSON), taskHandler::updateTaskById)
        DELETE("$tasksBase/{id}", accept(APPLICATION_JSON), taskHandler::deleteTaskById)

        POST(attemptsBase, accept(APPLICATION_JSON), attemptHandler::submitAttempt)

        GET(accountBase, accept(APPLICATION_JSON), accountHandler::getAccount)
        POST("/login", accept(APPLICATION_JSON), credentialsHandler::login)
        POST("/register", accept(APPLICATION_JSON), credentialsHandler::register)
    }
}